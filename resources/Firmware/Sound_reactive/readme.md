# Sound reactive fork

- [Repository](https://github.com/atuline/WLED) - Fork under development. All questions to WLED Discord #soundreactive

## Firmware for ESP32 and ESP8266

[ESP32 Sound reactive for shields](https://github.com/srg74/WLED-wemos-shield/tree/master/resources/Firmware/Sound_reactive/v0.13.2.1) - v0.13.2.1 build 2208181

[ESP32 Sound reactive for shields](https://github.com/srg74/WLED-wemos-shield/tree/master/resources/Firmware/Sound_reactive/v0.13.2-b1) - v0.13.2-b1 build 2208051

[ESP32 Sound reactive for shields](https://github.com/srg74/WLED-wemos-shield/tree/master/resources/Firmware/Sound_reactive/v0.13.0-b6) - v0.13.0-b6 build 2203061

[ESP32 Sound reactive for shields](https://github.com/srg74/WLED-wemos-shield/tree/master/resources/Firmware/Sound_reactive/v0.12.0) - v0.12.0
